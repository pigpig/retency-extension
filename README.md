# Contexte

Extension basée sur le projet "Retency" de Mindiell (https://framagit.org/Mindiell/retency)

Suite au dossier du RAP sur les écrans publicitaires de la RATP (https://antipub.org/dossier-les-cameras-publicitaires-pur-fantasme/), j'ai décidé de créer un petit script permettant de signaler à la société Retency chargée du projet une liste d'adresses MAC (adresse physique d'un périphérique réseau, comme une puce WiFi) au hasard afin de déclarer un maximum de périphériques comme ne devant pas être tracés.

# Explications

Lorsque vous faites une requête vers duckduckgo.com, l'extension génère une adresse MAC au hasard et la soumet à la société.

C'est le navigateur de l'internaute qui pousse les données, ce qui permet d'avoir un grand potentiel et peu de risque d'être contré, car il  s'agira de tas d'adresses IP différentes.

# Licence

L'ensemble de ce projet est ditribué sous la licence AGPL V3+.

